from urllib.request import urlopen
from bs4 import BeautifulSoup

failtext = "XX : Not Found"

#http://ponos.s3.dualstack.ap-northeast-1.amazonaws.com/information/appli/battlecats/gacha/rare/en/R315.html

def getname(code,verbose=False):
    try :
        url = "http://ponos.s3.dualstack.ap-northeast-1.amazonaws.com/information/appli/battlecats/stage/en/"+code+".html"
        html = urlopen(url).read()
        soup = BeautifulSoup(html, features="html.parser")
        
        # kill all script and style elements
        for script in soup(["script", "style"]):
            script.extract()    # rip it out
        
        # get text
        text = soup.get_text()
        
        # break into lines and remove leading and trailing space on each
        lines = (line.strip() for line in text.splitlines())
        # break multi-headlines into a line each
        chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
        # drop blank lines
        text = [chunk for chunk in chunks if chunk]
        if verbose:
            return text
        if text[1][1:] == "Total Stages":
            return "Unknown"
        return text[1]
    except:
        return failtext
    
prefixes = [('A', 27),('B', 14),('C', 2),('CA', 24),('H', 25),('M', 12),('N', 0),('NA', 13),('S', 1),('V', 7)]
for pfr in prefixes:
    n=0
    go = True
    print('Category - ', pfr)
    while go == True:
        x = getname(pfr[0]+format(n,"03d"))
        if x == failtext:
            go = False
        else:
            print(str(pfr[1])+format(n,"03d")+", "+x)  # 27 shouldn't be set like this
            n+=1
            
# A -> barons
# B -> catamins
# C -> collabs
# CA -> baron collab
# E -> followup stages (not used here, but prefix is 4)
# H -> enigma
# M -> challange battle
# N -> SoL
# NA -> UL
# S -> event stages (the red ones)
# V -> heavenly tower
# legend quest is 16