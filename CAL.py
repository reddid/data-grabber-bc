import csv, urllib.request, datetime
from _overlapped import NULL
from future.backports.xmlrpc.client import _strftime

eventNames = {}
codes = ["all"]
for code in codes:
    with open(code+'Events.csv', 'r', encoding='utf-8') as csvfile:
          reader = csv.reader(csvfile, delimiter=",")
          for row in reader:
              eventNames[row[0]] = row[1][1:]
with open('IDlist.csv', 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile, delimiter=",")
      for row in reader:
          eventNames[row[0]] = row[1]

def getEventName(ID):
    try:
        return eventNames[ID]
    except:
        return ID+" Unknown"

url = 'https://bc-seek.godfat.org/seek/en/sale.tsv'
response = urllib.request.urlopen(url)
lines = [l.decode('utf-8') for l in response.readlines()]
cr = csv.reader(lines, delimiter="\t")
eventdata = []
for row in cr:
    if len(row) > 1:
        eventdata.append(row)
        if len(row) > 1 and row[1] == "0":
            row[1] = "0000"
        if len(row) > 3 and row[3] == "0":
            row[3] = "0000"
            
url = 'https://bc-seek.godfat.org/seek/en/gatya.tsv'
response = urllib.request.urlopen(url)
lines = [l.decode('utf-8') for l in response.readlines()]
cr = csv.reader(lines, delimiter="\t")
gatya = []
for row in cr:
    if len(row) > 1:
        gatya.append(row)
        if len(row) > 1 and row[1] == "0":
            row[1] = "0000"
        if len(row) > 3 and row[3] == "0":
            row[3] = "0000"
    
def formatDate(s):
    return datetime.datetime.strptime(s, '%Y%m%d%H%M')

def formatMDHM(s,t):
    if t == "2400":
        t = "2359"
    if t == "0":
        t = "0000"
    if len(s) == 3:  # fixes date if month isn't nov or dec
        s = '0' + s
    return datetime.datetime.strptime(s+t, '%m%d%H%M')

def formatTime(t):
    if t == "2400":
        t = "2359"
    if t == "0":
        t = "0000"
    if len(t) == 3:  #fixes time if hour is < 10am
        t = '0' + t
    dt = datetime.datetime.strptime(t, '%H%M')
    return datetime.time(dt.hour,dt.minute)
    
def getdates(data):
    return formatDate(data[0]+data[1]),formatDate(data[2]+data[3])

def getversions(data):
    return data[4], data[5]

def binaryweekday(N):
    list_to_return = list(bin(N))[2:][::-1]
    while len(list_to_return)<7:
        list_to_return.append(0)
    return list_to_return

def yearly(data):
    numberOfPeriods = int(data[7])
    n = 8
    output = [dict() for x in range(numberOfPeriods)]
    IDs = []
    for i in range(numberOfPeriods):
        
        times, n = int(data[n]), n+1
        output[i]["times"] = [dict() for x in range(times)]
        
        for j in range(times):
            startDate, n = data[n], n+1
            startTime, n = data[n], n+1
            endDate, n = data[n], n+1
            endTime, n = data[n], n+1
            output[i]["times"][j]["start"] = formatMDHM(startDate,startTime)
            output[i]["times"][j]["end"] = formatMDHM(endDate,endTime)
            if output[i]["times"][j]["end"] < output[i]["times"][j]["start"]:  
                # this means the event ends the next year, like it starts on christmas and lasts for 2 weeks
                output[i]["times"][j]["end"] = output[i]["times"][j]["end"].replace(year=1901)
                
        
        n = n + 3 #trailing zeros
    
    nIDs, n = int(data[n]), n+1
    for k in range(max(nIDs,1)):
        ID, n = int(data[n]), n+1
        if nIDs > 0:
            IDs.append(ID)
            
    return output, IDs

def monthly(data):
    numberOfPeriods = int(data[7])
    n = 9
    output = [dict() for x in range(numberOfPeriods)]
    IDs = []
    for i in range(numberOfPeriods):
        
        dates, n = int(data[n]), n+1
        output[i]["dates"] = [""]*int(dates)
        for u in range(int(dates)):
            output[i]["dates"][u], n = data[n], n+1
        
        n = n+1 #Trailing zero
        
        times, n = int(data[n]), n+1
        output[i]["times"] = [dict() for x in range(times)]
        
        
        for j in range(times):
            start, n = data[n], n+1
            end, n = data[n], n+1
            output[i]["times"][j]["start"] = formatTime(start)
            output[i]["times"][j]["end"] = formatTime(end)
    
        nIDs, n = int(data[n]), n+1
        for k in range(nIDs):
            ID, n = int(data[n]), n+1
            if nIDs > 0:
                IDs.append(ID)
            
    return output, IDs

def weekly(data):
    numberOfPeriods = int(data[7])
    n = 10
    output = [dict() for x in range(numberOfPeriods)]
    IDs = []
    for i in range(numberOfPeriods):
        
        weekdays, n = binaryweekday(int(data[n])), n+1
        output[i]["weekdays"] = weekdays
        times, n = int(data[n]), n+1
        output[i]["times"] = [dict() for x in range(times)]
        
        for j in range(times):
            start, n = data[n], n+1
            end, n = data[n], n+1
            output[i]["times"][j]["start"] = formatTime(start)
            output[i]["times"][j]["end"] = formatTime(end)
        
        nIDs, n = int(data[n]), n+1
        
        for k in range(max(nIDs,1)):
            ID, n = int(data[n]), n+1
            if nIDs > 0:
                IDs.append(ID)
    
    return output, IDs

def daily(data):
    numberOfPeriods = int(data[7])
    n = 11
    output = [dict() for x in range(numberOfPeriods)]
    IDs = []
    for i in range(numberOfPeriods):
        
        times, n = int(data[n]), n+1
        output[i]["times"] = [dict() for x in range(times)]
        
        for j in range(times):
            startTime, n = data[n], n+1
            endTime, n = data[n], n+1
            output[i]["times"][j]["start"] = formatTime(startTime)
            output[i]["times"][j]["end"] = formatTime(endTime)
    
    nIDs, n = int(data[n]), n+1
    for k in range(max(nIDs,1)):
        ID, n = int(data[n]), n+1
        if nIDs > 0:
            IDs.append(ID)
    
    return output, IDs

refinedData = []

for data in eventdata:
    #permanent - just ID - all day
    if data[7] == '0':
        refinedData.append({
            "dates": getdates(data),
            "versions": getversions(data),
            "schedule": "permanent", 
            "IDs": data[9:9+int(data[8])]
            })
    #Yearly repeat XY - starts and ends at a date+time
    elif data[8] != '0':
        ydata, yIDs = yearly(data)
        refinedData.append({
            "dates": getdates(data),
            "versions": getversions(data),
            "schedule": "yearly", 
            "data": ydata,
            "IDs": yIDs
            })
    #Monthly repeat X0Y - list of days of month, may have time range
    elif data[9] != '0':
        mdata, mIDs = monthly(data)
        refinedData.append({
            "dates": getdates(data),
            "versions": getversions(data),
            "schedule": "monthly", 
            "data": mdata,
            "IDs": mIDs
            })
    #Weekly repeat X00Y - list of weekdays, may have time ranges
    elif data[10] != '0':
        wdata, wIDs = weekly(data)
        refinedData.append({
            "dates": getdates(data),
            "versions": getversions(data),
            "schedule": "weekly",
            "data": wdata,
            "IDs": wIDs
            })
    #Daily repeat X000Y - list of time ranges every day in interval
    elif data[11] != '0':
        ddata, dIDs = daily(data)
        refinedData.append({
            "dates": getdates(data),
            "versions": getversions(data),
            "schedule": "daily", 
            "data": ddata,
            "IDs": dIDs
            })
    else:
        print(data)

refinedGatya = []

def getpage(banner):
    p = banner[8]
    if p == "0":
        return "Cat Capsule" #Normal, lucky, G, catseye, catfruit, etc : uses GatyaDataSetN1.csv
    if p == "1":
        return "Rare Capsule" #Rare, platinum : uses GatyaDataSetR1.csv

    return "Event Capsule" #Some other page like bikkuri choco shit, etc, or some old-ass banners that don't exist now idk

def getValueAtOffset(banner, i): #i is the column ID for First Slot banners
    slot = int(banner[9])-1
    offset = 15*slot #Second slot data is 15 columns after first slot, and so on
    return banner[i + offset]

def getGatyaRates(banner):
    rates = []
    for i in range(5):
        rates.append(getValueAtOffset(banner, 14 + 2*i)) #14,16,18.20,22
    return rates

def getGatyaG(banner):
    G = []
    for i in range(5):
        G.append(getValueAtOffset(banner, 15 + 2*i)) #15,17,19,21,23
    return G
        
for banner in gatya:
    refinedGatya.append({
        "dates": getdates(banner),
        "versions": getversions(banner),
        "page": getpage(banner), #Is it on rare gacha page, silver ticket page, etc - affects the track it uses
        "slot": banner[9], #It is first, second, third etc from left to right in that page (also affects how to read rest of data)
        "Banner ID": getValueAtOffset(banner, 10), #The ID in the relevant GatyaDataSet csv
        #IDs 11,12,13 are random shit like roll cost, useless info, or stuff I forgot the meaning of
        #Only thing relevant I might be forgetting is if step up is set in here? Maybe godfat, jones or IT remember?
        "Rates": getGatyaRates(banner), #[Normal, Rare, Super, Uber, Legend] - rates out of 10000
        "Guarantee": getGatyaG(banner), #[Normal, Rare, Super, Uber, Legend] - 0 is no, 1 is yes,
        "Text": getValueAtOffset(banner, 24)
        })

def outputName(ID):
    name = getEventName(ID)
    if name != "Unknown" and name != ID+" Unknown":
        return name
    else:
        unknowns.append(ID)
        
def printnm(name, tag=""):  # print if not None
    if name != None:
        print(name+tag)

allIDs = []
for event in refinedData:
    for ID in event["IDs"]:
        allIDs.append(ID)

inputdate = datetime.datetime.today().replace(microsecond = 0,second = 0,minute=0, hour=0)  # might want to crop it to minutes

endofday = formatDate(inputdate.strftime("%Y%m%d")+"2359")
startofday = formatDate(inputdate.strftime("%Y%m%d")+"0000")

#Get all events not ended yet, or starting tomorrow or later

activeEvents = []
unknowns = []
for event in refinedData:
    start, end = event["dates"]
    if end > startofday and start <= endofday:
        activeEvents.append(event)

alldayEvents = []
endingtodayEvents = []       
eventsAtHour = [[] for x in range(24)]

for event in activeEvents:  # TODO implement yearly, monthly, weekly and daily
    start, end = event["dates"]
    if event["schedule"] == "permanent":
        if end > endofday and start <= startofday:
            alldayEvents.append(event)
        elif end < endofday and start <= startofday:
            endingtodayEvents.append(event)
        else:
            eventsAtHour[start.hour].append(event)
    elif event["schedule"] == "yearly":
        pass
    elif event["schedule"] == "monthly":
        pass
    elif event["schedule"] == "weekly":
        pass
    elif event["schedule"] == "daily":
        pass
    

# alldayEvents = []
# for event in activeEvents:
#     start, end = event["dates"]
#     if event["schedule"] == "permanent":
#         if end > formatDate(inputdate+"2359") and start <= formatDate(inputdate+"0000"):
#             alldayEvents.append(event)

#reddid here, i didn't touch your stuff so i'm using mine
events_inday=[[]]
for i in range(23):  # one for each hour
    events_inday.append([])

#get all day events
print("----")
print("ALL DAY")
print("----")
for event in alldayEvents:
    for ID in event["IDs"]:
        printnm(outputName(ID))
print("----")
print("ENDING TODAY")
print("----")
for event in endingtodayEvents:
    for ID in event["IDs"]:
        printnm(outputName(ID),tag = event["dates"][1].strftime(" (%H:%M)"))
print("----")
print("HOURLY SCHEDULE")
print("----")
for i in range(len(eventsAtHour)):     
    print(formatTime(str(i)+"00").strftime("- %H:%M -"))
    for event in eventsAtHour[i]:
        etag = " ("+str(event["dates"][1] - event["dates"][0])+")"
        for ID in event["IDs"]:
            printnm(outputName(str(ID)),tag = etag)

alldayEventsreddid=[]
print("reddid test")

#TODO we could definitely use a function which prints a given stage only if it belongs (or doesn't) to certain set of ID

for event in activeEvents:  # all events which are active during this period
    if event["schedule"] == 'daily':
        for ID in event["IDs"]:
            events_inday[event['data'][0]['times'][0]['start'].hour%24].append(outputName(str(ID)) + ' starts now.')
            events_inday[event['data'][0]['times'][0]['end'].hour%24].append(outputName(str(ID)) + ' ends now.')
    if event["schedule"] == 'weekly':
        for day_schedule in event["data"]:
            if day_schedule['weekdays'][inputdate.weekday()] == '1':  # happens at least today (of week)
                for instance_in_a_day in day_schedule['times']:  # for each time it happens in the same day
                    for ID in event["IDs"]:  # for every event whoch shares the scheduling
                        events_inday[(instance_in_a_day['start'].hour)%24].append(outputName(str(ID)) + ' starts now.')
                        events_inday[(instance_in_a_day['end'].hour)%24].append(outputName(str(ID)) + ' ends now.')
    elif event["schedule"] == 'monthly':
        if str(inputdate.day) in event['data'][0]['dates']:  # happens at least this day of the month
            if len(event['data'][0]['times']) > 0:  # event isn't all day
                for instance_in_a_day in event['data'][0]['times']: # for each time it happens in the same day
                    for ID in event["IDs"]: # for every event which shares the scheduling
                        events_inday[(instance_in_a_day['start'].hour)%24].append(outputName(str(ID)) + ' starts now.')
                        events_inday[(instance_in_a_day['end'].hour)%24].append(outputName(str(ID)) + ' ends now.')
            else:
                for ID in event["IDs"]:  # event is on all day and thus I append it to alldays events
                    alldayEventsreddid.append(outputName(str(ID)))
    elif event["schedule"] == 'yearly':
        dayoftheyear = inputdate.replace(year = 1900)  # adjusted date to make comparisons
        if event['data'][0]['times'][0]['start'] < dayoftheyear and event['data'][0]['times'][0]['end'] > dayoftheyear:  # are we in the middle of the event? (note that it's currenty too accurate and might get stuff wrong)
            for ID in event["IDs"]:  # event is on all day and thus I append it to alldays events
                alldayEventsreddid.append(outputName(str(ID)))
        elif event['data'][0]['times'][0]['start'] == dayoftheyear:  # begins today
            for ID in event["IDs"]: # for every event which shares the scheduling
                events_inday[event['data'][0]['times'][0]['start'].hour%24].append(outputName(str(ID)) + ' starts now.')  # untested
        elif event['data'][0]['times'][0]['end'] == dayoftheyear:  # ends today
            for ID in event["IDs"]: # for every event which shares the scheduling
                events_inday[event['data'][0]['times'][0]['end'].hour%24].append(outputName(str(ID)) + ' ends now.')  # untested
    elif event["schedule"] == 'permanent':
        for ID in event["IDs"]:  # event is on all day, basically forever
            alldayEventsreddid.append(outputName(str(ID)))
alldayEventsreddid = [x for x in alldayEventsreddid if x]  # removing None, since we don't like them, but we can't tell apart stuff which we don't know from the stuff the game doesn't know about

print('Events for today: ' + str(alldayEventsreddid))  # posting results
for i in range(24):
    print('Hour ' + str(i) + ': ' + str(events_inday[i]))

# but what if we wanted to see all of what happens with the current schedule?

ignored_ids = None  # init to something, might as well not waste memory

with open('ignoredIDs.csv', 'r', encoding='utf-8') as csvfile:  # I store ids of stuff which isn't important here
    reader = csv.reader(csvfile)
    ignored_ids = list(reader)[0]  # we only need the first line
print('\nIn schedule\n')
for event in refinedData:  # all events, doesn't matter if it's not active at the time of running
    for ID in event["IDs"]:  # all ids
        if str(ID) not in ignored_ids:  # don't print stuff we don't want
            print(event['dates'][0].strftime('%b %d'),'|',event['dates'][1].strftime('%b %d'),'|',outputName(str(ID)))  # formatted for reddit
