import csv
from BCHelpers import pathtofile, ENJPfolders
import os

(EN,JP) = ENJPfolders()

resLocal_EN = os.path.join(EN,"resLocal")
DataLocal_EN = os.path.join(EN,"DataLocal")
resLocal_JP = os.path.join(JP,"resLocal")
DataLocal_JP = os.path.join(JP,"DataLocal")

names_EN = []
itemd_EN = []
names_JP = []
itemd_JP = []


with open(pathtofile(DataLocal_JP,'Gatyaitembuy.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
          itemd_JP.append(row)
          
with open(pathtofile(resLocal_JP,'GatyaitemName.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile, delimiter = ",")
      for row in reader:
          names_JP.append(row[0])
      
with open(pathtofile(DataLocal_EN,'Gatyaitembuy.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
          itemd_EN.append(row)
          
with open(pathtofile(resLocal_EN,'GatyaitemName.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.reader(csvfile, delimiter = "|")
      for row in reader:
          if len(row) > 0:
              names_EN.append(row[0])
          
n = 0          
for row in itemd_JP:
    row["ID"] = n
    
    if n >= len(names_JP):
        names_JP.append("")
    
    try:
        row["Name"] = names_EN[n]
    except:
        row["Name"] = ""
        
    if row["Name"] == "":
        try:
            row["Name"] = names_JP[n]
        except:
            pass

    n += 1
    
with open('ItemData.csv', 'w', newline='', encoding='utf-8') as output:
    writer = csv.writer(output)
    for item in itemd_JP:
        writer.writerow([
            item["ID"],item["Name"],item["Rarity"],item["stageDropItemID"]
            ])