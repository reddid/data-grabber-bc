import csv
from Enemyname import createEnemyNames
from BCHelpers import pathtofile, ENJPfolders
import os

def getHeaders():
    (EN,JP) = ENJPfolders()
    
    h = []
    with open(pathtofile(os.path.dirname(JP),'tunit_headers.csv'), 'r') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            h.append(row[0])
    return h
            
            
def annotate():     
    tunit = []
    with open(pathtofile('DataLocal','t_unit.csv'), 'r', encoding='utf-8') as csvfile:
          reader = csv.reader(csvfile)
          for row in reader:
              try:
                  tunit.append(row)
              except IndexError:
                  tunit.append("")        
    
    headers = getHeaders()
    
    headers = headers + [""]*(len(tunit[0]) - len(tunit))
    
    annotated = [headers] + tunit
    
    names = createEnemyNames();
    
    for n in range(2, len(tunit)):
        try:
            annotated[n+1] = [names[n-2]] + annotated[n+1]
        except:
            print(n)
        
    with open('t_unit_annotated.csv', 'w', newline='', encoding='utf-8') as output:
        writer = csv.writer(output)
        for row in annotated:
            writer.writerow(row)
    
    return annotated