import csv
from BCHelpers import pathtofile, ENJPfolders
import os

(EN,JP) = ENJPfolders()

DataLocal_EN = os.path.join(EN,"DataLocal")
DataLocal_JP = os.path.join(JP,"DataLocal")

maps_EN = []
maps_JP = []


with open(pathtofile(DataLocal_JP,'Map_option.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
          maps_JP.append(row)
          
with open(pathtofile(DataLocal_EN,'Map_option.csv'), 'r', encoding='utf-8') as csvfile:
      reader = csv.DictReader(csvfile)
      for row in reader:
          maps_EN.append(row)
          
TLkeys = {
    'stageID' : "ID", 
    '星解放' : "Stars", 
    '星1倍率' : "Star1Mag", 
    '星2倍率' : "Star2Mag", 
    '星3倍率' : "Star3Mag", 
    '星4倍率' : "Star4Mag", 
    'ゲリラset' : "Guerilla Set", 
    '報酬リセットType' : "Reward Reset Type", 
    '1度きり表示' : "One Time Only", 
    '表示順' : "Display Order", 
    'インターバル' : "Interval", 
    '挑戦フラグ' : "Challenge Flag",  #Baron Cooldown
    'マップ名' : "Name"
    }

for d in maps_JP:
    for key in TLkeys.keys():
        d[TLkeys[key]] = d.pop(key)
for d in maps_EN:
    for key in TLkeys.keys():
        d[TLkeys[key]] = d.pop(key)
        
#MAPS - map-option for stars etc, with ID number #####
#MapStageDataA_XXX - list of stages in map, with Letter + Number
#stageAXXX_BB - stage BB in map A_XXX
#drops are in drop_chara/items and MapStageData says which one